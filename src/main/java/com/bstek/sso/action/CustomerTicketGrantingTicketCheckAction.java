/**
 * 
 */
package com.bstek.sso.action;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.ticket.AbstractTicketException;
import org.jasig.cas.ticket.Ticket;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.bstek.sso.utils.CacheUtils;
/**
 * @author michael
 *
 * 2017年12月19日
 */
@Component("customerTicketGrantingTicketCheck")
public class CustomerTicketGrantingTicketCheckAction extends AbstractAction {

    /**
     * TGT does not exist event ID={@value}.
     **/
    public static final String NOT_EXISTS = "notExists";

    /**
     * TGT invalid event ID={@value}.
     **/
    public static final String INVALID = "invalid";

    /**
     * TGT valid event ID={@value}.
     **/
    public static final String VALID = "valid";

    /**
     * The Central authentication service.
     */
    @NotNull
    private final CentralAuthenticationService centralAuthenticationService;


    /**
     * Creates a new instance with the given ticket registry.
     *
     * @param centralAuthenticationService the central authentication service
     */
    @Autowired
    public CustomerTicketGrantingTicketCheckAction(@Qualifier("centralAuthenticationService")
                                               final CentralAuthenticationService centralAuthenticationService) {
        this.centralAuthenticationService = centralAuthenticationService;
    }

    /**
     * Determines whether the TGT in the flow request context is valid.
     *
     * @param requestContext Flow request context.
     *
     * @throws Exception in case ticket cannot be retrieved from the service layer
     * @return {@link #NOT_EXISTS}, {@link #INVALID}, or {@link #VALID}.
     */
    @Override
    protected Event doExecute(final RequestContext requestContext) throws Exception {
		final String tgtId = WebUtils.getTicketGrantingTicketId(requestContext);
		if (!StringUtils.hasText(tgtId)) {
			return new Event(this, NOT_EXISTS);
		}
		String systemCode = requestContext.getRequestParameters().get("systemCode");
		if(systemCode==null){
			return new Event(this,INVALID);
		}
		@SuppressWarnings("unchecked")
		List<String> codes =(List<String>) CacheUtils.getSysytemCodeMap(tgtId);
		if(!codes.contains(systemCode)){
			return new Event(this,INVALID);
		}
		String eventId = INVALID;
		try {
			final Ticket ticket = this.centralAuthenticationService.getTicket(tgtId, Ticket.class);
			if (ticket != null && !ticket.isExpired()) {
				eventId = VALID;
			}
		} catch (final AbstractTicketException e) {
			logger.trace("Could not retrieve ticket id {} from registry.", e);
		}
		return new Event(this, eventId);
    }
}
