/**
 * 
 */
package com.bstek.sso.model;

import java.util.List;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jasig.cas.authentication.UsernamePasswordCredential;

/**
 * @author michael
 *
 *         2017年12月12日
 */
public class UsernamePasswordSystemCodeCredential extends UsernamePasswordCredential {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5497340710985389136L;
	private String systemCode;
	private List<String> systemCodeList;
	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}	
	public List<String> getSystemCodeList() {
		return systemCodeList;
	}
	public void setSystemCodeList(List<String> systemCodeList) {
		this.systemCodeList = systemCodeList;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().appendSuper(super.hashCode()).append(systemCode).toHashCode();
	}
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UsernamePasswordSystemCodeCredential other = (UsernamePasswordSystemCodeCredential) obj;
		if (this.systemCode != other.systemCode) {
			return false;
		}
		return true;
	}

}
