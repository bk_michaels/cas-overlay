/**
 * 
 */
package com.bstek.sso.utils;

import java.net.URL;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * @author michael
 *
 * 2017年12月21日
 */
public class CacheUtils {
	
	private static Cache getCache() {
		URL url=null;
		try {
			url = Class.forName("com.bstek.sso.utils.CacheUtils").getResource("/cache/ehcache.xml");
			CacheManager cacheManager = CacheManager.create(url);
			Cache cache = cacheManager.getCache("systemCodeCache");
			return cache;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void putSysytemCodeMap(String key, Object value) {
		Cache cache = getCache();
		Element e = new Element(key, value);
		cache.put(e);
	}

	public static Object getSysytemCodeMap(String key) {
		Cache cache = getCache();
		Element e = cache.get(key);
		return  e.getObjectValue();
	}
	public static void clearCache(String key){
		Cache cache = getCache();
		cache.remove(key);
	}
	

}
