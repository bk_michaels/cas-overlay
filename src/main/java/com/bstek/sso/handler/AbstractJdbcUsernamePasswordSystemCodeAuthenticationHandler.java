/**
 * 
 */
package com.bstek.sso.handler;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author michael
 *
 * 2017年12月13日
 */
public abstract class AbstractJdbcUsernamePasswordSystemCodeAuthenticationHandler extends
AbstractUsernamePasswordSystemCodeAuthenticationHandler {
	
	    private JdbcTemplate jdbcTemplate;

	    private DataSource dataSource;

	    /**
	     * Method to set the datasource and generate a JdbcTemplate.
	     *
	     * @param dataSource the datasource to use.
	     */
	    public void setDataSource(@NotNull final DataSource dataSource) {
	        this.jdbcTemplate = new JdbcTemplate(dataSource);
	        this.dataSource = dataSource;
	    }

	    /**
	     * Method to return the jdbcTemplate.
	     *
	     * @return a fully created JdbcTemplate.
	     */
	    protected final JdbcTemplate getJdbcTemplate() {
	        return this.jdbcTemplate;
	    }

	    protected final DataSource getDataSource() {
	        return this.dataSource;
	    }

}
